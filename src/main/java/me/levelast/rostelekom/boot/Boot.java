package me.levelast.rostelekom.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by v.svininykh
 */
@EnableAutoConfiguration
@ComponentScan(basePackages = "me/levelast/rostelekom")
public class Boot {
    public static void main(String[] args) {
        SpringApplication.run(Boot.class, args);
    }
}
