package me.levelast.rostelekom.controller;

import me.levelast.rostelekom.model.RequestDetail;
import me.levelast.rostelekom.service.IRequestDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by v.svininykh
 */
@RestController
public class RequestController {

    @Autowired
    IRequestDetailService requestDetailService;

    @RequestMapping(value = "/request_detail", produces = MediaType.TEXT_XML_VALUE)
    public RequestDetail getRequestDetail(HttpServletRequest request) {
        return requestDetailService.getRequestDetail(request);
    }
}
