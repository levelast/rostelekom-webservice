package me.levelast.rostelekom.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

/**
 * Created by v.svininykh
 */
public class ClientInfo {

    @JacksonXmlProperty(localName = "ip-address")
    private String ipAddress;
    @JacksonXmlProperty(localName = "user-agent")
    private String userAgent;

    public ClientInfo() {
    }

    public ClientInfo(String ipAddress, String userAgent) {
        this.ipAddress = ipAddress;
        this.userAgent = userAgent;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }
}
