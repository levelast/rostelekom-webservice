package me.levelast.rostelekom.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by v.svininykh
 */
public class QueryParameters {

    @JacksonXmlProperty(localName = "method", isAttribute = true)
    private String method;
    @JacksonXmlElementWrapper(localName = "numeric_parameters")
    @JacksonXmlProperty(localName = "num_parameter")
    private List<Parameter<BigDecimal>> numericParameters;
    @JacksonXmlElementWrapper(localName = "string_parameters")
    @JacksonXmlProperty(localName = "str_parameter")
    private List<Parameter<String>> stringParameters;

    public QueryParameters() {
    }

    public QueryParameters(String method, List<Parameter<BigDecimal>> numericParameters, List<Parameter<String>> stringParameters) {
        this.method = method;
        this.numericParameters = numericParameters;
        this.stringParameters = stringParameters;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public List<Parameter<BigDecimal>> getNumericParameters() {
        return numericParameters;
    }

    public void setNumericParameters(List<Parameter<BigDecimal>> numericParameters) {
        this.numericParameters = numericParameters;
    }

    public List<Parameter<String>> getStringParameters() {
        return stringParameters;
    }

    public void setStringParameters(List<Parameter<String>> stringParameters) {
        this.stringParameters = stringParameters;
    }

}
