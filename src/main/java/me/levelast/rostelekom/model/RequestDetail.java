package me.levelast.rostelekom.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;

/**
 * Created by v.svininykh
 */
@JacksonXmlRootElement(localName = "request_detail")
public class RequestDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @JacksonXmlProperty(localName = "client_info")
    private ClientInfo clientInfo;
    @JacksonXmlProperty(localName = "parameters")
    private QueryParameters queryParameters;

    public RequestDetail() {
    }

    public RequestDetail(ClientInfo clientInfo, QueryParameters queryParameters) {
        this.clientInfo = clientInfo;
        this.queryParameters = queryParameters;
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }

    public QueryParameters getQueryParameters() {
        return queryParameters;
    }

    public void setQueryParameters(QueryParameters queryParameters) {
        this.queryParameters = queryParameters;
    }
}
