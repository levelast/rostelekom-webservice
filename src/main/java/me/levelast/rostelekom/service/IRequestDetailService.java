package me.levelast.rostelekom.service;

import me.levelast.rostelekom.model.RequestDetail;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by v.svininykh
 */
public interface IRequestDetailService {

    RequestDetail getRequestDetail(HttpServletRequest request);
}
