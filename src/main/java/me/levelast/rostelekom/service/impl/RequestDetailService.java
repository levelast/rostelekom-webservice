package me.levelast.rostelekom.service.impl;

import me.levelast.rostelekom.model.QueryParameters;
import me.levelast.rostelekom.model.ClientInfo;
import me.levelast.rostelekom.model.Parameter;
import me.levelast.rostelekom.model.RequestDetail;
import me.levelast.rostelekom.service.IRequestDetailService;
import me.levelast.rostelekom.utils.Utils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by v.svininykh
 */
@Service
public class RequestDetailService implements IRequestDetailService {

    @Override
    public RequestDetail getRequestDetail(HttpServletRequest request) {

        ClientInfo clientInfo = new ClientInfo(request.getRemoteAddr(), request.getHeader("User-Agent"));

        List<Parameter<BigDecimal>> numericParameters = new ArrayList<>();
        List<Parameter<String>> stringParameters = new ArrayList<>();
        for (Map.Entry<String, String[]> entry: request.getParameterMap().entrySet()) {
            String value = entry.getValue()[0];
            if (Utils.isNumeric(value)) {
                numericParameters.add(new Parameter<>(entry.getKey(), Utils.stringToNumeric(value)));
            } else {
                stringParameters.add(new Parameter<>(entry.getKey(), value));
            }
        }

        QueryParameters queryParameters = new QueryParameters(request.getMethod(), numericParameters, stringParameters);
        return new RequestDetail(clientInfo, queryParameters);
    }
}
