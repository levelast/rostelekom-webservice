package me.levelast.rostelekom.utils;

import java.math.BigDecimal;
import java.util.regex.Pattern;

/**
 * Created by v.svininykh
 */
public class Utils {

    public static boolean isNumeric(String str) {
        Pattern pattern = Pattern.compile("^-?[0-9]{1,10}([,.][0-9]{0,10})?$");
        return pattern.matcher(str).matches();
    }

    public static BigDecimal stringToNumeric(String str) {
        return new BigDecimal(str.replace(",", "."));
    }
}
