package me.levelast.rostelekom.service.impl;

import me.levelast.rostelekom.boot.Boot;
import me.levelast.rostelekom.model.Parameter;
import me.levelast.rostelekom.model.RequestDetail;
import me.levelast.rostelekom.service.IRequestDetailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by v.svininykh
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Boot.class)
public class RequestDetailServiceTest {

    @Autowired
    private IRequestDetailService requestDetailService;

    @Test
    public void testGetRequestDetailByIp() {
        MockHttpServletRequest requestMock = new MockHttpServletRequest("GET", "/request_detail");
        requestMock.setRemoteAddr("156.213.169.61");
        RequestDetail requestDetail = requestDetailService.getRequestDetail(requestMock);
        assertEquals(requestMock.getRemoteAddr(), requestDetail.getClientInfo().getIpAddress());
    }

    @Test
    public void testGetRequestDetailByUserAgent() {
        MockHttpServletRequest requestMock = new MockHttpServletRequest("GET", "/request_detail");
        requestMock.addHeader("User-Agent", "Test_Agent");
        RequestDetail requestDetail = requestDetailService.getRequestDetail(requestMock);
        assertEquals(requestMock.getHeader("User-Agent"), requestDetail.getClientInfo().getUserAgent());
    }

    @Test
    public void testGetRequestDetailByGetMethod() {
        MockHttpServletRequest requestMock = new MockHttpServletRequest("GET", "/request_detail");
        RequestDetail requestDetail = requestDetailService.getRequestDetail(requestMock);
        assertEquals("GET", requestDetail.getQueryParameters().getMethod());
    }

    @Test
    public void testGetRequestDetailByPostMethod() {
        MockHttpServletRequest requestMock = new MockHttpServletRequest("POST", "/request_detail");
        RequestDetail requestDetail = requestDetailService.getRequestDetail(requestMock);
        assertEquals("POST", requestDetail.getQueryParameters().getMethod());
    }

    // Здесь конечно надо расскидать всё по разным тестам
    @Test
    public void testGetRequestDetailByParameters() {
        MockHttpServletRequest requestMock = new MockHttpServletRequest("GET", "/request_detail");
        Map<String, String> params = new LinkedHashMap<>();
        params.put("param1", "235423");
        params.put("param2", "asdasd");
        params.put("param3", "1153254326.34543");
        params.put("param4", "1153254326,34543");
        params.put("param5", "1153254326.3454345634634664");
        params.put("param6", "3454345634634664.34234");
        params.put("param7", "345.34.234");
        params.put("param8", "345..234");
        params.put("param9", "-56.98");
        params.put("param10", "");

        requestMock.setParameters(params);
        RequestDetail requestDetail = requestDetailService.getRequestDetail(requestMock);
        List<Parameter<BigDecimal>> numericParameters = requestDetail.getQueryParameters().getNumericParameters();
        List<Parameter<String>> stringParameters = requestDetail.getQueryParameters().getStringParameters();

        assertEquals("param1", numericParameters.get(0).getName());
        assertEquals(new BigDecimal("235423"), numericParameters.get(0).getValue());

        assertEquals("param3", numericParameters.get(1).getName());
        assertEquals(new BigDecimal("1153254326.34543"), numericParameters.get(1).getValue());

        assertEquals("param4", numericParameters.get(2).getName());
        assertEquals(new BigDecimal("1153254326.34543"), numericParameters.get(2).getValue());

        assertEquals("param9", numericParameters.get(3).getName());
        assertEquals(new BigDecimal("-56.98"), numericParameters.get(3).getValue());

        assertEquals("param2", stringParameters.get(0).getName());
        assertEquals("asdasd", stringParameters.get(0).getValue());

        assertEquals("param5", stringParameters.get(1).getName());
        assertEquals("1153254326.3454345634634664", stringParameters.get(1).getValue());

        assertEquals("param6", stringParameters.get(2).getName());
        assertEquals("3454345634634664.34234", stringParameters.get(2).getValue());

        assertEquals("param7", stringParameters.get(3).getName());
        assertEquals("345.34.234", stringParameters.get(3).getValue());

        assertEquals("param8", stringParameters.get(4).getName());
        assertEquals("345..234", stringParameters.get(4).getValue());

        assertEquals("param10", stringParameters.get(5).getName());
        assertEquals("", stringParameters.get(5).getValue());
    }

    @Test
    public void testGetRequestDetailWithoutParameters() {
        MockHttpServletRequest requestMock = new MockHttpServletRequest("GET", "/request_detail");
        RequestDetail requestDetail = requestDetailService.getRequestDetail(requestMock);
        assertTrue(requestDetail.getQueryParameters().getNumericParameters().size() == 0);
        assertTrue(requestDetail.getQueryParameters().getStringParameters().size() == 0);
    }
}
